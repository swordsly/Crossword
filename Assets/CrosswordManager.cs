﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CrosswordManager : MonoBehaviour {

    [SerializeField]
    InputField[] _inputs;
    [SerializeField]
    Image[] _grids;
    [SerializeField]
    Button _solveBtn;
    [SerializeField]
    Image _completed;

    [Space(10f)]
    [SerializeField]
    Color _chosenCharColor;

    public void GeneratePuzzle () {
        StopAllCoroutines();
        ResetBoard();
        bool __tryAgain = false;
        int __tries = 0;
        do {
            ClearData();
            try {
                __tries++;
                __tryAgain = Setup();
            } catch (Exception e) {
                print(e.Message);
            }
        } while (!__tryAgain);
        //Debug.Log(string.Format("Total tries: {0}", __tries));
        PrintGrids();
        PopulateBoard();
        _boardGRC.enabled = true;
        _solveBtn.interactable = true;
        StartCoroutine(RunCheckRoutine());
    }

    public void SolvePuzzle () {
        StopAllCoroutines();
        for (int i = 0; i < _usedWords.Count; i++) {
            for (int j = 1; j < _usedWords[i].length; j++) {
                int __gridIndex = 0;
                if (_usedWords[i].isVertical)
                    __gridIndex = 10 * (_usedWords[i].coordinate.y + j) + _usedWords[i].coordinate.x;
                else 
                    __gridIndex = 10 * _usedWords[i].coordinate.y + (_usedWords[i].coordinate.x + j);
                _inputs[__gridIndex].text = _usedWords[i].word[j].ToString();
            }
        }
        _boardGRC.enabled = false;
        _solveBtn.interactable = false;
    }

    void Start () {
        _boardGRC = transform.GetChild(0).GetComponent<GraphicRaycaster>();
        GeneratePuzzle();
    }

    void PopulateBoard () {
        int __index = 0;
        for (int i = 0; i < _usedWords.Count; i++) {
            __index = 10 * _usedWords[i].coordinate.y + _usedWords[i].coordinate.x;
            for (int j = 0; j < _usedWords[i].length; j++) {
                int __gridIndex = 0;
                if (_usedWords[i].isVertical) {
                    __gridIndex = 10 * (_usedWords[i].coordinate.y + j) + _usedWords[i].coordinate.x;
                    _inputs[__gridIndex].interactable = true;
                    _grids[__gridIndex].color = Color.white;
                } else {
                    __gridIndex = 10 * _usedWords[i].coordinate.y + (_usedWords[i].coordinate.x + j);
                    _inputs[__gridIndex].interactable = true;
                    _grids[__gridIndex].color = Color.white;
                }
            }
            _inputs[__index].text = _chosenChar.ToString().ToLower();
            _inputs[__index].textComponent.color = _chosenCharColor;
            _inputs[__index].interactable = false;
        }
    }

    void ResetBoard () {
        for (int i = 0; i < _inputs.Length; i++) {
            _inputs[i].text = string.Empty;
            _inputs[i].textComponent.color = Color.black;
            _inputs[i].interactable = false;
            _grids[i].color = Color.clear;
        }
        _completed.enabled = false;
    }

    void ClearData () {
        _board = null;
        _workingStack.Clear();
        _usedWords.Clear();
        _wordDict.Clear();
        _discardList.Clear();
    }

    bool Setup () {
        _width = Random.Range(8, 11);
        _height = Mathf.Min(_width + Random.Range(-1, 2), 10);
        _board = new char[_width * _height];
        Char __char = _chosenChar;
        if (__char == '\0') {
            _chosenChar = (char)('A' + Random.Range(0, 26));
        } else {
            do {
                _chosenChar = (char)('A' + Random.Range(0, 26));
            } while (_chosenChar == __char);   
        }
        _totalWords = Random.Range(4, 6);
        // max length to be the shorter side of the board
        _maxWordLength = (_width > _height ? _height : _width) + 1;

        try {
            string __wordInFile = string.Empty;
            using (FileStream fs = new FileStream(string.Format(TEXT_FILE_PATH, _chosenChar), FileMode.Open, FileAccess.Read)) {
                using (StreamReader sr = new StreamReader(fs, Encoding.UTF8)) {
                    do {
                        __wordInFile = sr.ReadLine();
                        if (__wordInFile.Length < MIN_WORD_LENGTH || __wordInFile.Length > _maxWordLength)
                            continue;
                        if (_wordDict.ContainsKey(__wordInFile.Length)) {
                            _wordDict[__wordInFile.Length].Add(__wordInFile);
                        } else {
                            _wordDict.Add(__wordInFile.Length, new List<string>());
                            _wordDict[__wordInFile.Length].Add(__wordInFile);
                        }

                    } while (sr.ReadLine() != null);
                }
            }
        } catch (Exception e) {
            print(string.Format("Error Message: {0}\nError Source: {1}", e.Message, e.Source));
        }

        // initial orientation
        _isVertical = _width > _height;

        int __wordCount = 0;
        WordData __currentData = new WordData();
        int __currentWordLength = 0;
        string __searchedWord = string.Empty;

        // process word0
        // set starting coordinate
        if (_isVertical)
            __currentData.coordinate = new Vector2Int(Random.Range(3, _width), 0);
        else
            __currentData.coordinate = new Vector2Int(0, Random.Range(3, _height));
        // random word length (longer than normally)
        __currentWordLength = Random.Range(Mathf.CeilToInt(_maxWordLength / 2), _maxWordLength);

        SearchForWord(ref __searchedWord, __currentWordLength);
        __currentData.word = __searchedWord;
        __currentData.length = __currentWordLength;
        //Debug.Log(__currentData.coordinate);
        WriteToBoard(__searchedWord, __currentData.coordinate.x, __currentData.coordinate.y, __currentData.length);
        __currentData.isVertical = _isVertical;

        // post word handling
        _workingStack.Push(__currentData);
        _usedWords.Add(__currentData);
        _discardList.Add(__searchedWord);
        __searchedWord = string.Empty;
        __wordCount++;

        // process word1 to word(n-1) where n = total words
        for (;__wordCount < _totalWords; __wordCount++) {
            __currentData = new WordData();
            ProcessWordN(ref __searchedWord, ref __currentData);
            __currentData.word = __searchedWord;
            __currentData.length = __searchedWord.Length;
            //Debug.Log(__currentData.coordinate);
            WriteToBoard(__searchedWord, __currentData.coordinate.x, __currentData.coordinate.y, __currentData.length);
            __currentData.isVertical = _isVertical;

            // post word handling
            _workingStack.Push(__currentData);
            _usedWords.Add(__currentData);
            _discardList.Add(__searchedWord);
            __searchedWord = string.Empty;
        }

        if (_usedWords.Count < _totalWords)
            return false;
        return true;
    }

    void ProcessWordN (ref string wordContainer, ref WordData data) {
        WordData __peekedData;
        string __word = string.Empty;
        do {
            __peekedData = _workingStack.Peek();
            //Debug.Log(__peekedData.ToString());
            // process working word
            _isVertical = !__peekedData.isVertical;
            bool __isWordFound = false;
            // look through all characters in word
            int __position = 0;
            int __availableGrids = 0;
            bool __isIncreasing = false;
            for (; __position < __peekedData.length; __position++) {
                //Debug.Log(string.Format("{0} == {1} => {2}", __peekedData.word[__position], _chosenChar, __peekedData.word[__position] == _chosenChar));
                __isIncreasing = __peekedData.word[__position] == _chosenChar;
                // get maximum length in increasing direction with word[i] as starting point
                // get maximum length in decreasing direction with word[i] as ending point
                if (__peekedData.isVertical)
                    __availableGrids = GetAvailableGrids(__peekedData.coordinate.x, __peekedData.coordinate.y + __position, __isIncreasing);
                else
                    __availableGrids = GetAvailableGrids(__peekedData.coordinate.x + __position, __peekedData.coordinate.y, __isIncreasing);
                //Debug.Log("Available grids: " + __availableGrids);
                if (__availableGrids >= MIN_WORD_LENGTH) {
                    if (__peekedData.word[__position] == _chosenChar)
                        __isWordFound = SearchForWord(ref __word, __availableGrids);
                    else
                        __isWordFound = SearchForWord(ref __word, __availableGrids, __peekedData.word[__position]);
                }
                if (__isWordFound)
                    break;
            }
            //Debug.Log(string.Format("Word found: {0}", __word));
            //Debug.Log("Word found: " + __isWordFound);
            if (__isWordFound) {
                wordContainer = __word;
                if (__isIncreasing) {
                    data.coordinate = __peekedData.coordinate;
                } else {
                    if (_isVertical)
                        data.coordinate = new Vector2Int(__position, __peekedData.coordinate.y - __availableGrids + 1);
                    else
                        data.coordinate = new Vector2Int(__peekedData.coordinate.x - __availableGrids + 1, __position);
                }
                break;
            }
            // word doesn't yield possible results, dump it
            _workingStack.Pop();
        } while (string.IsNullOrEmpty(wordContainer));
    }

    int GetAvailableGrids (int startX, int startY, bool isIncreasing) {
        //Debug.Log(string.Format("Increasing? {0} Vertical? {1}", isIncreasing, _isVertical));
        int __availableGrids = 1;
        if (isIncreasing) {
            if (_isVertical) {
                for (int i = startY + 1; i < _height; i++) {
                    if (i >= _height)
                        break;
                    int __index = _width * i + startX;
                    if (_board[__index] == '+')
                        break;
                    if (_board[__index] == '-' || _board[__index] == '\0') {
                        __availableGrids++;
                        continue;
                    }
                    __availableGrids--;
                    break;
                }
            } else {
                for (int i = startX + 1; i < _width; i++) {
                    if (i >= _width)
                        break;
                    int __index = _width * startY + i;
                    if (_board[__index] == '+')
                        break;
                    if (_board[__index] == '-' || _board[__index] == '\0') {
                        __availableGrids++;
                        continue;
                    }
                    __availableGrids--;
                    break;
                }
            }
        } else {
            if (_isVertical) {
                for (int i = startY - 1; i >= 0; i--) {
                    if (i < 0)
                        break;
                    int __index = _width * i + startX;
                    if (_board[__index] == '+')
                        break;
                    if (_board[__index] == '-' || _board[__index] == '\0') {
                        __availableGrids++;
                        continue;
                    }
                    __availableGrids--;
                    break;
                }
            } else {
                for (int i = startX - 1; i >= 0; i--) {
                    if (i < 0)
                        break;
                    int __index = _width * startY + i;
                    if (_board[__index] == '+')
                        break;
                    if (_board[__index] == '-' || _board[__index] == '\0') {
                        __availableGrids++;
                        continue;
                    }
                    __availableGrids--;
                    break;
                }
            }
        }
        return __availableGrids;
    }

    bool SearchForWord (ref string word, int wordLength) {
        if (_wordDict.ContainsKey(wordLength)) {
            List<string> __results = new List<string>();
            for (int i = 0; i < _wordDict[wordLength].Count; i++) {
                if (_discardList.Count <= 0) {
                    __results.Add(_wordDict[wordLength][i]);
                }
                else if (!_discardList.Contains(_wordDict[wordLength][i])) {
                    __results.Add(_wordDict[wordLength][i]);
                }
            }
            if (__results.Count == 1) {
                word = __results[0];
                return true;
            }
            if (__results.Count > 1) {
                word = __results[Random.Range(0, __results.Count)];
                return true;
            }
        }
        return false;
    }

    bool SearchForWord (ref string word, int wordLength, char fixedChar) {
        if (_wordDict.ContainsKey(wordLength)) {
            List<string> __results = new List<string>();
            for (int i = 0; i < _wordDict[wordLength].Count; i++) {
                if (_wordDict[wordLength][i][wordLength - 1] == fixedChar) {
                    if (_discardList.Count <= 0) {
                        __results.Add(_wordDict[wordLength][i]);
                    }
                    else if (!_discardList.Contains(_wordDict[wordLength][i])) {
                        __results.Add(_wordDict[wordLength][i]);
                    }
                }
            }
            if (__results.Count == 1) {
                word = __results[0];
                return true;
            }
            if (__results.Count > 1) {
                word = __results[Random.Range(0, __results.Count)];
                return true;
            }
        }
        return false;
    }

    void WriteToBoard (string word, int startX, int startY, int wordLength) {
        int __index = _width * startY + startX;
        int __moveBy = (_isVertical) ? _width : 1;
        //Debug.Log(string.Format("index: {0}, word: {1}", __index, word));
        for (int i = 0; i < wordLength; i++) {
            _board[__index] = word[i];
            __index += __moveBy;
        }

        if (_isVertical) {
            // top neighbour
            if (startY - 1 >= 0)
                SetNeighbouringGrid(_width * (startY - 1) + startX);
            
            // bottom neighbour
            if (startY + wordLength < _height)
                SetNeighbouringGrid(_width * (startY + wordLength) + startX);
            
            // side neighbours
            for (int i = 0; i < wordLength; i++) {
                // left neighbour
                if (startX - 1 >= 0) 
                    SetNeighbouringGrid(_width * (startY + i) + (startX - 1));
                
                // right neighbour
                if (startX + 1 < _width) 
                    SetNeighbouringGrid(_width * (startY + i) + (startX + 1));
            }
        } else {
            // left neighbour
            if (startX - 1 >= 0)
                SetNeighbouringGrid(_width * startY + (startX - 1));
            
            // right neighbour
            if (startX + wordLength < _width) 
                SetNeighbouringGrid(_width * startY + (startX + wordLength));
            
            // side neighbours
            for (int i = 0; i < wordLength; i++) {
                // top neighbour
                if (startY - 1 >= 0) 
                    SetNeighbouringGrid(_width * (startY - 1) + (startX + i));
                
                // bottom neighbour
                if (startY + 1 < _height) 
                    SetNeighbouringGrid(_width * (startY + 1) + (startX + i));
            }
        } 
    }

    void SetNeighbouringGrid (int gridIndex) {
        if (_board[gridIndex] == '\0')
            _board[gridIndex] = '-';
        else if (_board[gridIndex] == '-')
            _board[gridIndex] = '+';
    }

    IEnumerator RunCheckRoutine () {
        do {
            int __totalCorrect = 0;
            for (int i = 0; i < _usedWords.Count; i++) {
                int __correct = 0;
                for (int j = 1; j < _usedWords[i].length; j++) {
                    int __gridIndex = 0;
                    if (_usedWords[i].isVertical)
                        __gridIndex = 10 * (_usedWords[i].coordinate.y + j) + _usedWords[i].coordinate.x;
                    else
                        __gridIndex = 10 * _usedWords[i].coordinate.y + (_usedWords[i].coordinate.x + j);
                    
                    if (_inputs[__gridIndex].text.Equals(_usedWords[i].word[j].ToString()))
                        __correct++;
                }
                if (__correct == _usedWords[i].length - 1)
                    __totalCorrect++;
            }
            if (__totalCorrect == _usedWords.Count)
                break;
            yield return _checkWait;
        } while (true);
        _completed.enabled = true;
        _boardGRC.enabled = false;
        _solveBtn.interactable = false;
    }

    void PrintGrids () {
        string __result = string.Empty;
        __result += string.Format("ANSWER\nwidth: {0}, height : {1}\n\n", _width, _height);
        for (int i = 0; i < _board.Length; i++) {
            if (_board[i] == '\0')
                __result += '*' + " ";
            else
                __result += _board[i].ToString() + " ";
            if (i % _width == _width - 1)
                __result += "\n";
        }
        print(__result);
    }

    void Reset () {
        _inputs = transform.Find("GridCanvas").GetComponentsInChildren<InputField>(true);
        _grids = new Image[_inputs.Length];
        for (int i = 0; i < _grids.Length; i++) {
            _grids[i] = _inputs[i].transform.parent.GetComponent<Image>();
        }
    }

    bool _isVertical;

    int _width;
    int _height;

    int _totalWords;
    int _maxWordLength;

    char _chosenChar;
    char[] _board;

    Dictionary<int, List<string>> _wordDict = new Dictionary<int, List<string>>();
    Stack<WordData> _workingStack = new Stack<WordData>();
    List<WordData> _usedWords = new List<WordData>();
    List<string> _discardList = new List<string>();

    GraphicRaycaster _boardGRC;

    WaitForSeconds _checkWait = new WaitForSeconds(3f);

    static int MIN_WORD_LENGTH = 3;
    static string TEXT_FILE_PATH = @"Assets/Resources/{0}.txt";
}

public class WordData {

    public Vector2Int coordinate;
    public int length;
    public string word;
    public bool isVertical;

    public override string ToString () {
        return string.Format("Word: {0}, Coordinate: {1}, Vertical: {2}", word, coordinate, isVertical);
    }

}
